import Vue from 'vue';
import Router from 'vue-router';
import Vuetify from 'vuetify';
import Form from './../components/Form';

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Form',
      component: Form
    },
  ],
});
