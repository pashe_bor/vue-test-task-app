import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

const state = {
  title: 'Test store text',
  numbers: 0,
};

const mutations = {
  SET_NUMBERS: (state, number) => {
    state.numbers = number;
  },
};

const actions = {
  setNumbers: ({ commit }, number) => {
    commit('SET_NUMBERS', number);
  },
};

const getters = {

};

export default {
  state, mutations, actions, getters,
};
